#!/usr/bin/env python

def funcion_retorno(params):
	""" retorno valore de un diccionario """
	return "|||||".join(["%s=%s" % (k,v) for k,v in params.items()])



if __name__ == "__main__":
	myParams = {"server" : "Apache",
				"ip" : "127.0.0.1",
				"so" : "osx"}  

	print funcion_retorno(myParams)