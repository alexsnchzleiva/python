#!/usr/bin/env python

def info(object, spacing=10, collapse=11):
	""" Imprime metodos y doc Coje los modulos, clases, listas, diccionarios ...."""

	methodList = [method for method in dir(object) if callable(getattr(object, method))]

	processFunc = collapse and (lambda s: "".join(s.split())) or (lambda s:s)

	print "\n".join(["%s %s" % (method.ljust(spacing), processFunc(str(getattr(object, method).__doc__)))
	 	for method in methodList])





class Prueba(object):
	"""docstring for Prueba"""

	def __init__(self, arg):
		super(Prueba, self).__init__()
		self.arg = arg

	def saluda():
		print "Saluda"
		


p = Prueba(object)

if __name__ == "__main__":
	info(p)
	print "---------------------"
	li = ["Hola"]
	info(li)
	print dir(li)

	print getattr(li, "pop")
	print li

	getattr(li,"append")("Adios")
	print li



