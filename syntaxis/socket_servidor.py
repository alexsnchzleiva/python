#!/usr/bin/env python


import socket

s = socket.socket()
s.bind(("127.0.0.1",9000))

s.listen(10)

sc, addr = s.accept()


while True:
    recibido = sc.recv(1024)
    
    if recibido == "quit":
        break
    print "Recibido: ", recibido
    sc.send(recibido)
print "Adios"

sc.close()
s.close()