

def funcionSimple(a, b = "Mundo", *c):
	"""Esto es el doc """
	print a
	print b

	for val in c:	
		print val


funcionSimple("Hola", "Python", 1,2,3,4)



def functionReturn():
	return 2


valor = functionReturn()

print valor



def saluda(saludo,nombre):
	print "%s %s" % (saludo,nombre)


saluda(nombre = "Hola", saludo = "Alex")