#!/usr/bin/env python

import sqlite3 as dbapi

print dbapi.apilevel
print dbapi.threadsafety
print dbapi.paramstyle


try:
    bbdd = dbapi.connect("bbdd.dat")
    print bbdd
    
    cursor = bbdd.cursor()
    print cursor
    
    cursor.execute("create table prueba (nombre text) ")
    cursor.execute("insert into prueba values ('alejandro') ")
    bbdd.commit()
    
    nombre = 'alejandro'
    cursor.execute(" select * from prueba where nombre = ? ", (nombre,) )

    
    for tupla in cursor:
        print tupla
        
    
    cursor.close()
    bbdd.close()
    
except StandardError:
    print "Error"