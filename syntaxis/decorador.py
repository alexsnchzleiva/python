#!/usr/bin/env python

def decorador(funcion):
    
    def funcionModificada(*args):
        print "Funciones que se ejecuta: " + funcion.__name__
        retorno = funcion(*args)
        return retorno
    return funcionModificada


def saluda():
    print "Hola"


decorador(saluda)()




#Segunda Prueba, Forma correcta

def funcionDecorador(function):
    def funcionEnvoltura():
        print "Antes de la funcion"
        function()
        print "Despues de la funcion"
    return funcionEnvoltura

@funcionDecorador
def funcionDecorada():
    print "Ejecutando"
    
funcionDecorada()

