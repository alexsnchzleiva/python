#!/usr/bin/env python

#serializacion de un objeto

import shelve

try:
    import cPinckle as pickle
except ImportError:
    import pickle
    

#guardamos el fichero    

fichero = file("datos.dat","w")
animales = ["piton","mono","camello"]

pickle.dump(animales,fichero,2)
fichero.close()



#y lo recuperamos

fichero = file("datos.dat")
animales2 = pickle.load(fichero)
print animales2


#No funciona

ficheroLenguajes = file("ficheroLenguajes.dat","w")
lenguajes = ["C++","Java","Python"]
pickle.dump(lenguajes,ficheroLenguajes,2)

s = shelve.open("ficheroLenguajes.dat")
s["primera"] = lenguajes

print s["primera"]

s.close()




               