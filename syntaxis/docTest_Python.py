
import doctest

def suma(a,b):
    """ Calcula los valores pasados como parametros 
    
    >>> c = 2
    >>> d = 4
    >>> suma(c,d)
    6
    """
    
    return a + b


def _test():
    import doctest
    doctest.testmod()


if __name__ == "__main__":
    _test()

