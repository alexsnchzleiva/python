#!/usr/bin/env python


import urllib, urllib2
from urllib2 import HTTPError, URLError

#Obtener datos de una url
try:
    f = urllib2.urlopen("http://alexsleiva.com")
    print f.read()
    f.close()
except HTTPError:
    print HTTPError
except URLError:
    print URLError
    
    
#Anyadir parametros GET a una peticion
params = urllib.urlencode({"usuario" : "alexsleiva"})

f = urllib2.urlopen("http://alexsleiva.com/",params)
print f.read()



#Descarga lo que contiene una URL
try:
    f = urllib.urlretrieve("http://alexsleiva.com","./urlretive.txt")
    f.close()
except Exception:
    print "Error"
    
    
    
#Llamadas usando objeto Request
ua = "Mozilla/5.0 (compatible; Konqueror/3.5.8; Linux)"
h = {"User-Agent": ua}
r = urllib2.Request("http://alexsleiva.com", headers=h)
f = urllib2.urlopen(r)
print f.read()





