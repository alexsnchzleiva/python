#!/usr/bin/env python

import threading
import urllib

semaforo = threading.Semaphore(2)

def descargar(url):
    semaforo.acquire()
    urllib.urlretrieve(url,"t.txt")
    semaforo.release()
    
    
descargar("http://google.com")
