#!/usr/bin/env python


class EntidadBase(object):
	"""Doc EntidadBase"""
	
	def __init__(self):
		pass
	
	def nombreCreacion(self):
		print "fechaCreacion"
	
class EntidadAuxiliar(object):
	"""Doc EntidadAuxiliar"""
	
	def __init__(self):
		pass
	
	def fechaCreacion(self):
		"""fecha Creacion Doc"""
		print "fechaCreacion"

	
class Prueba(EntidadBase, EntidadAuxiliar):
	"Doc clase Prueba"
	
	def __init__(self, nombre):
		self.nombre = nombre
	
	def saluda(self):
		print "Saludos de " + self.nombre
	
	

prueba = Prueba("Alex")
prueba.saluda()
prueba.nombreCreacion()
print prueba.__doc__
print help(prueba.fechaCreacion)


	
