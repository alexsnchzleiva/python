#!/usr/bin/env python

def saludar(lenguaje):
    """ Doc saludar """
    
    def saludar_es():
        print "Hola"
    
    def saludar_en():
        print "Hi"
    
    lenguaje_funcion = {"es" : saludar_es,
                        "en" : saludar_en}  
    
    return lenguaje_funcion[lenguaje]
        
    
s = saludar("en")
s()    
    
    
    

