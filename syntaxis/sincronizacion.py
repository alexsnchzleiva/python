#!/usr/bin/env python

import threading

lista = []

lock = threading.Lock()


def anyadir(obj):
    lock.acquire()
    lista.append(obj)
    lock.release()
    
def obtener():
    lock.acquire()
    if len(lista) > 0:
        obj = lista.pop()
    lock.release()
    return obj



for i in range(0,900):
    anyadir(i)
    print obtener()


