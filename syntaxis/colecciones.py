#!usr/bin/python

#Listas

listaSimple = ["Primero","Segundo","Tercero","Cuarto"]

print listaSimple[-1]

listaSimple.append("Quinto")
listaSimple.insert(0,"Cero")
listaSimple.extend(["Sexto"])
listaSimple.pop()
listaSimple.remove("Cero")

print listaSimple.count("Primero")
print listaSimple.index("Tercero")
listaSimple.reverse()

listaCompuesta = [[1,2,3],[4,5,6]]

print(listaSimple)

print(listaCompuesta)

print(listaCompuesta[0][0:2])

tuple(listaCompuesta)


#Tupla o Arrays

tuplaSimple = (1,2,3,4,5,6,7,8,9)
tuplaText = "Hola Python"

print(tuplaSimple)
print(tuplaText[0:11:2])

list(tuplaSimple)



#Diccionarios

diccionarioSimple = {1 : "Perdrosa" , 2 : "Lorenzo"}

if diccionarioSimple.has_key(1):
    print(diccionarioSimple.get(1))
else:
    print("No existe")
        
        
if 1 in diccionarioSimple:
    print("Existe")
else:
    print("No existe")
    
    
diccionarioSimple.items()
diccionarioSimple.keys()
diccionarioSimple.values()

for i in diccionarioSimple.items():
    print i

for i in diccionarioSimple.keys():
    print i

del(diccionarioSimple[2]) 
diccionarioSimple.clear()

for i in diccionarioSimple.values():
    print i
    

lista = ["Alex","Jorge","Juan"]
print lista
print "\n".join(lista)

