#!/usr/bin/env python

try:
    f = file("bucles.py")
    print f
except IOError:
    print "IOError"
except Exception:
    print "Exception"
else:
    print "OK"
finally:
    print "Finally"





class ExceptionMod(Exception):
    def __init__(self, valor):
        self.valor = valor
        
    def __str__(self):
        return "Error " + str(self.valor)
    

try:
    i = 1
    if i>0:
        raise ExceptionMod("Personalizado")
except ExceptionMod , e:
    print e
    
    
