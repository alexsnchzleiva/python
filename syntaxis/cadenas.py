#!/usr/bin/env python

from math import pi

cadena = "Hola-Mundo"

print cadena.count("o", 1,12)

print cadena.find("a")

print cadena.join("casa")

print cadena.partition("-")

print cadena.replace("M","m")

print cadena.split("-")

print "Python" + "Postgres"

print "Python", "Postgres"

print "Post%s" % "gres"

print "%s %s" % ("Hola","Python")

print "%10sHola" % "Mundo"

print "Hola %20s" % "Mundo"

print "%d" % pi

print "%.2f" % pi

s = "alex"
print s.ljust(30),"AA"

