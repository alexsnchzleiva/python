#!/usr/bin/env python

import threading


#Clase con Thread
class MiHilo(threading.Thread):
    def __init__(self,num):
        threading.Thread.__init__(self)
        self.num = num
    
    def run(self):
        print "Soy el hilo numero",self.num
        
        
class NuevoHilo(threading.Thread):
    def __init__(self, num):
        threading.Thread.__init__(self)
        self.num = num
    
    def run(self):
        print "Soy el hilo numero" , self.num
    
    
#Ejecucion de Thread
hilo1 = MiHilo(1)
hilo2 = MiHilo(2)
hilo3 = NuevoHilo(3)
hilo4 = NuevoHilo(4)

hilo1.start()
hilo2.start()
hilo3.start()
hilo4.start()




for aux in range(0,100):
    hilo1 = MiHilo("A")
    hilo2 = MiHilo("B")
    hilo3 = NuevoHilo("B")
    hilo4 = NuevoHilo("C")

    hilo1.start()
    hilo2.start()
    hilo3.start()
    hilo4.start()
    
    hilo1.join()
    hilo2.join()
    hilo3.join()
    hilo4.join()

