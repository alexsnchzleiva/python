#!/usr/bin/env python

import re


a = ".ython"
b = "Jython"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
a = "...\."
b = "abc."

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
a = "Python|Jython|Cython"
b = "Jython"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
a = "(P|J|C)ython"
b = "Jython"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
a = "[P|J|C]ython"
b = "Jython"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
    
a = "aaa[0-9a-zA-Z]"
b = "aaaW"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
a = "aaa[^0-9]"
b = "aaa2"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
a = "aaa[\d]"
b = "aaa2"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
a = "aaa[\w]"
b = "aaa2"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
a = "aaa[\s]"
b = "aaa "

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    

a = "1K+"
b = "1KKK"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    

a = "1K*A"
b = "1KKKKKKKKKA"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
a = "1K?A"
b = "1A"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"
    
    
    
    
    
    
    
a = "A1{0,3}A"
b = "A111A"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"




a = "^http://"
b = "http://marca.com"

if re.match(a,b):
    print "Cierto"
else:
    print "Falso"

