#!/usr/bin/env python

lista = [1,2,3,4,5]
listaComprimida = [aux for aux in lista if aux%2.0 == 0]
print listaComprimida

lista = [1,2,3,4,5]
listaComprimida = [aux -1 for aux in lista]
print listaComprimida

listaArriba = [1,2,3,4,5]
listaAbajo = ["a","b"]
listaComprimida = [arriba * abajo
                   for abajo  in listaAbajo
                   for arriba in listaArriba]

print listaComprimida

listaNueva = [10,20,30,40,50]
listaNuevaMulti = [aux * 2 for aux in listaNueva]
print listaNuevaMulti
