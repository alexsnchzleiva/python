#!/usr/bin/env python



#map

def cuadrado(n):
    return n * 3

def sumatorio(a,b):
    return a + b

lista = [1,2,3,4]
tupla = (5,6,7,8)

listaProcesada = map(cuadrado, lista)
print listaProcesada

listasUnidas = map(sumatorio,lista,tupla)
print listasUnidas




#filter

def esPar(numero):
    return (numero % 2.0 == 0)

lista = [1,2,3,4]

listaProcesada = filter(esPar,lista)
print listaProcesada




#reduce

def sumar(x,y):
    return x * y

lista = [1,2,4,6,8]
listaProcesada = reduce(sumar, listaProcesada)

print listaProcesada




