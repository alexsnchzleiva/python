#!/usr/bin/env python

import sys

if (len(sys.argv) > 1):
    print "Abriendo " + sys.argv[1]
else:
    print "Debe indicar el nombre del archivo"