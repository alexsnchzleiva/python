#!/usr/bin/env python

def generador(n,m,s):
    while(n <= m):
        yield n
        n += s
        
var_generador = generador(0,5,1)

print var_generador


for n in generador(0,15,3):
    print n
    
print list(generador(0,15,3))

